<?php
/**
 * Template Name: Register Template
 */
?>
<?php

// Add our custom loop
add_action( 'genesis_loop', 'cd_goh_loop' );
function cd_goh_loop() {
    if( have_posts() ) {
        // loop through posts
        while( have_posts() ): the_post();
        ?>
            <?php the_content(); ?>
        <?php
        endwhile;
    }
    wp_reset_postdata();
}

genesis();